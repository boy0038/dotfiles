# add cover to mp3 file in ../g/ folder
for a in *.mp3;
do         # $a = audio
 ffmpeg -i "$a" -i "IMAGE" -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" "../g/${a%.*}.mp3" -y
done


# create YT video with an image + audio --> to /f/ folder
for a in *.mp3;
do                                             # $a = audio
 ffmpeg -y -loop 1 -framerate 2 -i "IMAGE" -i "$a" -c:v libx264 -preset medium -tune stillimage -crf 18 -c:a copy -shortest -pix_fmt yuv420p "../f/${a%.*}.avi"
done