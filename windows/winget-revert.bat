@echo off
:: https://privacy.sexy — v0.11.4 — Thu, 06 Jul 2023 20:45:33 GMT
:: Ensure admin privileges
fltmc >nul 2>&1 || (
    echo Administrator privileges are required.
    PowerShell Start -Verb RunAs '%0' 2> nul || (
        echo Right-click on the script and select "Run as administrator".
        pause & exit 1
    )
    exit 0
)


:: ----------------------------------------------------------
:: --Disable Firewall through command-line utility (revert)--
:: ----------------------------------------------------------
echo --- Disable Firewall through command-line utility (revert)
PowerShell -ExecutionPolicy Unrestricted -Command "if(!(Get-Command 'netsh' -ErrorAction Ignore)) {; throw '"^""netsh"^"" does not exist, is system installed correctly?'; }; $message=netsh advfirewall set allprofiles state on 2>&1; if($?) {; Write-Host "^""Successfully enabled firewall."^""; } else {; if($message -like '*Firewall service*') {; Write-Warning 'Cannot use CLI because MpsSvc or MpsDrv is not running. Try to enable them (revert) -> reboot -> re-run this?'; } else {; throw "^""Cannot enable: $message"^""; }; }"
:: ----------------------------------------------------------


:: ----------------------------------------------------------
:: --------Disable Firewall through registry (revert)--------
:: ----------------------------------------------------------
echo --- Disable Firewall through registry (revert)
:: Policy based
reg delete "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile" /v "EnableFirewall" /f 2>nul
reg delete "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\PublicProfile" /v "EnableFirewall" /f 2>nul
reg delete "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\PrivateProfile" /v "EnableFirewall" /f 2>nul
reg delete "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" /v "EnableFirewall" /f 2>nul
:: Non-policy based
reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile" /v "EnableFirewall" /t REG_DWORD /d 1 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile" /v "EnableFirewall" /t REG_DWORD /d 1 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile" /v "EnableFirewall" /t REG_DWORD /d 1 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PrivateProfile" /v "EnableFirewall" /t REG_DWORD /d 1 /f
:: ----------------------------------------------------------


:: ----------------------------------------------------------
:: -Hide the "Firewall and network protection" area (revert)-
:: ----------------------------------------------------------
echo --- Hide the "Firewall and network protection" area (revert)
reg delete "HKLM\SOFTWARE\Policies\Microsoft\Windows Defender Security Center\Firewall and network protection" /v "UILockdown" /f 2>nul
:: ----------------------------------------------------------


:: Disable Windows Defender Firewall Authorization Driver service (breaks `netsh advfirewall` CLI) (revert)
echo --- Disable Windows Defender Firewall Authorization Driver service (breaks `netsh advfirewall` CLI) (revert)
PowerShell -ExecutionPolicy Unrestricted -Command "$serviceQuery = 'mpsdrv'; $defaultStartupMode = 'Manual'; <# -- 1. Skip if service does not exist #>; $service = Get-Service -Name $serviceQuery -ErrorAction SilentlyContinue; if(!$service) {; Write-Warning "^""Service query `"^""$serviceQuery`"^"" did not yield and results, cannot enable it."^""; Exit 1; }; $serviceName = $service.Name; Write-Host "^""Enabling service: `"^""$serviceName`"^"" with `"^""$defaultStartupMode`"^"" start."^""; <# -- 2. Skip if service info is not found in registry #>; $registryKey = "^""HKLM:\SYSTEM\CurrentControlSet\Services\$serviceName"^""; if(!(Test-Path $registryKey)) {; Write-Warning "^""`"^""$registryKey`"^"" is not found in registry, cannot enable it."^""; Exit 1; }; <# -- 3. Enable if not already enabled #>; $defaultStartupRegValue = if ($defaultStartupMode -eq 'Boot') { '0' } elseif($defaultStartupMode -eq 'System') { '1' } elseif($defaultStartupMode -eq 'Automatic') { '2' } elseif($defaultStartupMode -eq 'Manual') { '3' } else { throw "^""Unknown start mode: $defaultStartupMode"^""}; if( $(Get-ItemProperty -Path "^""$registryKey"^"").Start -eq $defaultStartupRegValue) {; Write-Host "^""`"^""$serviceName`"^"" is already enabled with `"^""$defaultStartupMode`"^"" start."^""; } else {; try {; Set-ItemProperty $registryKey -Name Start -Value $defaultStartupRegValue -Force; Write-Host "^""Enabled `"^""$serviceName`"^"" successfully with `"^""$defaultStartupMode`"^"" start, may require restarting your computer."^""; } catch {; Write-Error "^""Could not enable `"^""$serviceName`"^"": $_"^""; Exit 1; }; }; <# -- 4. Start if not running (must be enabled first) #>; if($defaultStartupMode -eq 'Automatic') {; if ($service.Status -ne [System.ServiceProcess.ServiceControllerStatus]::Running) {; Write-Host "^""`"^""$serviceName`"^"" is not running, trying to start it."^""; try {; Start-Service $serviceName -ErrorAction Stop; Write-Host "^""Started `"^""$serviceName`"^"" successfully."^""; } catch {; Write-Warning "^""Could not start `"^""$serviceName`"^"", requires restart, it will be started after reboot.`r`n$_"^""; }; } else {; Write-Host "^""`"^""$serviceName`"^"" is already running, no need to start."^""; }; }"
if exist "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD" (
    takeown /f "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD"
    icacls "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD" /grant administrators:F
    move "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD" "%SystemRoot%\System32\drivers\mpsdrv.sys" && (
        echo Moved "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD" to "%SystemRoot%\System32\drivers\mpsdrv.sys"
    ) || (
        echo Could restore from backup file %SystemRoot%\System32\drivers\mpsdrv.sys.OLD 1>&2
    )
) else (
    echo Could not find backup file "%SystemRoot%\System32\drivers\mpsdrv.sys.OLD" 1>&2
)
:: ----------------------------------------------------------


:: Disable Windows Defender Firewall service (breaks Microsoft Store and `netsh advfirewall` CLI) (revert)
echo --- Disable Windows Defender Firewall service (breaks Microsoft Store and `netsh advfirewall` CLI) (revert)
PowerShell -ExecutionPolicy Unrestricted -Command "$serviceQuery = 'MpsSvc'; $defaultStartupMode = 'Automatic'; <# -- 1. Skip if service does not exist #>; $service = Get-Service -Name $serviceQuery -ErrorAction SilentlyContinue; if(!$service) {; Write-Warning "^""Service query `"^""$serviceQuery`"^"" did not yield and results, cannot enable it."^""; Exit 1; }; $serviceName = $service.Name; Write-Host "^""Enabling service: `"^""$serviceName`"^"" with `"^""$defaultStartupMode`"^"" start."^""; <# -- 2. Skip if service info is not found in registry #>; $registryKey = "^""HKLM:\SYSTEM\CurrentControlSet\Services\$serviceName"^""; if(!(Test-Path $registryKey)) {; Write-Warning "^""`"^""$registryKey`"^"" is not found in registry, cannot enable it."^""; Exit 1; }; <# -- 3. Enable if not already enabled #>; $defaultStartupRegValue = if ($defaultStartupMode -eq 'Boot') { '0' } elseif($defaultStartupMode -eq 'System') { '1' } elseif($defaultStartupMode -eq 'Automatic') { '2' } elseif($defaultStartupMode -eq 'Manual') { '3' } else { throw "^""Unknown start mode: $defaultStartupMode"^""}; if( $(Get-ItemProperty -Path "^""$registryKey"^"").Start -eq $defaultStartupRegValue) {; Write-Host "^""`"^""$serviceName`"^"" is already enabled with `"^""$defaultStartupMode`"^"" start."^""; } else {; try {; Set-ItemProperty $registryKey -Name Start -Value $defaultStartupRegValue -Force; Write-Host "^""Enabled `"^""$serviceName`"^"" successfully with `"^""$defaultStartupMode`"^"" start, may require restarting your computer."^""; } catch {; Write-Error "^""Could not enable `"^""$serviceName`"^"": $_"^""; Exit 1; }; }; <# -- 4. Start if not running (must be enabled first) #>; if($defaultStartupMode -eq 'Automatic') {; if ($service.Status -ne [System.ServiceProcess.ServiceControllerStatus]::Running) {; Write-Host "^""`"^""$serviceName`"^"" is not running, trying to start it."^""; try {; Start-Service $serviceName -ErrorAction Stop; Write-Host "^""Started `"^""$serviceName`"^"" successfully."^""; } catch {; Write-Warning "^""Could not start `"^""$serviceName`"^"", requires restart, it will be started after reboot.`r`n$_"^""; }; } else {; Write-Host "^""`"^""$serviceName`"^"" is already running, no need to start."^""; }; }"
if exist "%WinDir%\system32\mpssvc.dll.OLD" (
    takeown /f "%WinDir%\system32\mpssvc.dll.OLD"
    icacls "%WinDir%\system32\mpssvc.dll.OLD" /grant administrators:F
    move "%WinDir%\system32\mpssvc.dll.OLD" "%WinDir%\system32\mpssvc.dll" && (
        echo Moved "%WinDir%\system32\mpssvc.dll.OLD" to "%WinDir%\system32\mpssvc.dll"
    ) || (
        echo Could restore from backup file %WinDir%\system32\mpssvc.dll.OLD 1>&2
    )
) else (
    echo Could not find backup file "%WinDir%\system32\mpssvc.dll.OLD" 1>&2
)
:: ----------------------------------------------------------


pause
exit /b 0