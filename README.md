## 40-libinput.conf
 + sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/

## bash-update.hook
 + sudo cp bash-update.hook /usr/share/libalpm/hooks/

## defaults.vim
 + /usr/share/vim/vim82/defaults.vim

## ntp.conf
 + sudo cp ntp.conf /etc/ntp.conf

## zathurarc
 + sudo cp zathurarc /etc/zathurarc
